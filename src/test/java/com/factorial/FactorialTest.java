package com.factorial;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

class FactorialTest {

    @Test
    public void shouldCalculateFactorialForZero() {
        //given
        Factorial factorial = new Factorial();

        //when
        long result = factorial.factorial(0);

        //then
        assertThat(result).isEqualTo(1);
    }

    @Test
    public void shouldCalculateFactorialForOne() {
        //given
        Factorial factorial = new Factorial();

        //when
        long result = factorial.factorial(1);

        //then
        assertThat(result).isEqualTo(1);
    }

    @Test
    public void shouldCalculateFactorial() {
        //given
        Factorial factorial = new Factorial();

        //when
        long result = factorial.factorial(9);

        //then
        assertThat(result).isEqualTo(362880);
    }

}
